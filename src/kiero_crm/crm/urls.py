""" URLS FOR INFORMATION OPERATOR """

from django.urls import path
from .views import *

app_name = 'CRM'

urlpatterns = [
    path(r'accounts/', AccountViewset.as_view({'post': 'create', "get": 'list'}), name='accounts'),
    path(r'accounts/email/', AccountDetailEmailViewset.as_view({'get': 'retrieve_with_email'}), name='accounts_retrieve_with_email'),
    path(r'accounts/<pk>/', AccountViewset.as_view({'put': 'partial_update', "get": 'retrieve', 'delete': 'destroy'}), name='accounts_detail'),
    path(r'address/', AddressViewset.as_view({'post': 'create', "get": 'list'}), name='address'),
    path(r'address/<pk>/', AddressViewset.as_view({'delete': 'destroy', "get": 'retrieve', 'put': 'partial_update'}), name='address'),
    path(r'plans/', PlansVieset.as_view({'post': 'create', "get": 'list'}), name='address'),
    path(r'plans/<pk>/', PlansVieset.as_view({'delete': 'destroy', "get": 'retrieve', 'put': 'partial_update'}), name='address'),
    path(r'otp/', OTPViewset.as_view({'post': 'create', "get": 'list'}), name='address'),
    path(r'otp/<pk>/', OTPViewset.as_view({'delete': 'destroy', "get": 'retrieve', 'put': 'partial_update'}), name='address'),
]