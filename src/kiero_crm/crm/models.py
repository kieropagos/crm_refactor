from django.db import models
import uuid
from .service import *

# Create your models here.

class Account(models.Model):

    DOC_TYPE_CHOICES=[
        (0, 'CC'),
        (1, 'CE'),
        (2, 'NIT'),
        (3, 'PS')
    ]

    ROLE_CHOICES=[
        (0, "client"),
        (1, "shop"),
        (2, "kiero"),
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    id_marketplace = models.IntegerField(unique=True, blank=True, null=True)
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    doc_type = models.IntegerField(choices=DOC_TYPE_CHOICES)
    document = models.CharField(max_length=40, unique=True)
    role = models.IntegerField(choices=ROLE_CHOICES)
    phone = models.IntegerField()
    is_active = models.BooleanField(default=True)
    shop = models.OneToOneField('Shop', on_delete=models.PROTECT, null=True, blank=True, unique=True)
    address = models.ManyToManyField('Address', blank=True)
    notification_active = models.BooleanField(default=True)
    wallet = models.BigIntegerField()
    otp = models.BooleanField(default=False)
    plan = models.CharField(max_length=225)

class Shop(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=100)

class Address(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    id_owner = models.IntegerField()
    name = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    address = models.CharField(max_length=10)
    street  = models.CharField(max_length=10)
    street_2  = models.CharField(max_length=10)
    house_number = models.CharField(max_length=10)
    add_info = models.CharField(max_length=225)
    contact_phone = models.IntegerField()

class Plans(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    cost = models.IntegerField()
    tax = models.IntegerField()
    text = models.CharField(max_length=225)

class OTP(models.Model):
    code = models.CharField(max_length=225, default=get_random_alphanumeric_string(8))
    owner = models.IntegerField()
    