from rest_framework import filters
import django_filters
from django_filters import Filter, FilterSet
from .models import *

class ListFilter(Filter):
    def filter(self, qs, value):
        if not value:
            return qs

        # For django-filter versions < 0.13, use lookup_type instead of lookup_expr
        self.lookup_expr = 'in'
        values = value.split(',')
        return super(ListFilter, self).filter(qs, values)

class AccountFilter(django_filters.FilterSet):

    role = ListFilter()

    class Meta:
        model = Account
        fields = '__all__'
        
