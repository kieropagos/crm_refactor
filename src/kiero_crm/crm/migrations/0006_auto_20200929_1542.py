# Generated by Django 3.1.1 on 2020-09-29 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0005_auto_20200929_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='id_marketplace',
            field=models.IntegerField(blank=True, null=True, unique=True),
        ),
    ]
