# Generated by Django 3.1.1 on 2020-10-02 15:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0006_auto_20200929_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='id_owner',
            field=models.IntegerField(blank=True, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='account',
            name='address',
            field=models.ManyToManyField(blank=True, to='crm.Address'),
        ),
    ]
