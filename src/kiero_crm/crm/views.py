from rest_framework import viewsets, status, response, pagination
from django.shortcuts import get_object_or_404
from .serializer import *
from .models import *
from .filters import *
from django_filters.rest_framework import DjangoFilterBackend

class AccountViewset(viewsets.ModelViewSet):

    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    pagination_class = pagination.LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filterset_class = AccountFilter
    filterset_fields = '__all__'

class AddressViewset(viewsets.ModelViewSet):

    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    pagination_class = pagination.LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'


class AccountDetailEmailViewset(viewsets.ViewSet):

    serializer_class = AccountSerializer

    def retrieve_with_email(self, request):
        params = request.query_params
        query = get_object_or_404(Account, email=params['value'])
        serializer_account = AccountSerializer(query)
        return response.Response(serializer_account.data, status=status.HTTP_200_OK)

class OTPViewset(viewsets.ModelViewSet):
    queryset = OTP.objects.all()
    serializer_class = OTPSerializer
    pagination_class = pagination.LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class PlansVieset(viewsets.ModelViewSet):
    queryset = Plans.objects.all()
    serializer_class = PlanSerializer
    pagination_class = pagination.LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'
