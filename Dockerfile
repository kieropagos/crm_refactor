FROM python:3.6

ENV PYTHONUNBUFFERED 1

COPY ./src/kiero_crm/ /code/
WORKDIR /code/

RUN pip install -r requirements.txt

EXPOSE 8000